package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

const (
	filename  = "example3"
	extension = "pdf"
)

func main() {
	// Открытие файла
	inFile, err := os.Open(fmt.Sprintf("%s.%s", filename, extension))
	if err != nil {
		log.Fatal(err)
	}
	defer inFile.Close()

	// Создание выходного файла
	outFile, err := os.Create(fmt.Sprintf("%s_mod.%s", filename, extension))
	if err != nil {
		log.Fatal(err)
	}
	defer outFile.Close()

	// Размер файла в байтах
	fileStats, err := inFile.Stat()
	if err != nil {
		log.Fatal(err)
	}

	// Инициализация слайса байтов
	byteArray := make([]byte, fileStats.Size())

	// Считывание файла в слайс
	_, err = bufio.NewReader(inFile).Read(byteArray)
	if err != nil {
		log.Fatal(err)
	}

	// Обработка файла с замером времени
	start := time.Now()
	processByteArray(byteArray)
	duration := time.Since(start)
	fmt.Printf("Время обработки файла в секундах: %f\n", duration.Seconds())

	// Запись в выходной файл
	_, err = outFile.Write(byteArray)
	if err != nil {
		log.Fatal(err)
	}
}

func processByteArray(byteArray []byte) {
	// Для генерации различных последовательностей случайных чисел при каждом запуске программы
	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)

	// Переменная y объявлена отдельно, т.к. иначе ругается компилятор
	var x, y int = 0, 256

	// Без приведения результата этого всего к byte компилятор отказывается это запускать
	for i := 0; i < len(byteArray); i++ {
		x = r.Intn(100)
		byteArray[i] += byte((i * x) & y)
	}
}
